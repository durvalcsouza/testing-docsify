# Documentação da Brelo

Os documentos desse site são voltados para os desenvolvedores da Brelo. O objetivo é reduzir a curva de aprendizado para que novos desenvolvedores e desenvolvedores de outro time possam rapidamente compreender o escopo do sistema, seus módulos, regras de negócios e decisões importantes que foram tomadas no passado.

### Mensagem para quem consome a documentação

Caso você não encontre um documento, uma informação ou identifique incoerencias em algum documento, crie uma issue notificando, sua colaboração irá ajudar a melhorar nossos documentos.

### Mensagem para quem produz os documentos

Durante a produção dos documentos, busque seguir os seguintes princípios:
- **Reader Needs**: Documentação é sobre ajudar quem está lendo, então antes de começar escrever, pense nos futuros leitores desse documento. O que eles estão querendo saber? Quais palabras chaves serão pesquisadas na busca? Quais perguntas seriam evitadas com esse documento? São algumas das perguntas que o autor da documentação deve se fazer.

- **Write less**: Documentos muito grandes são menos lidos e são mais difíceis de se manter. Nunca escreva mais documentação do que você é capaz de manter.

- **Write the outline first**: Antes de começar a escrever um documento, faça um esboço. Pense na estrutura do documento, os tópicos que serão cobertos. O uso de uma tabela de conteúdo no início do documento pode ser útil para estruturar o documento.

- **Rubber ducking**: Busque diminuir o nível de formalidade do documento. Tente escrever os documentos como se estivesse realizando um diálogo com o leitor. Documentos escritos dessa maneira são mais intuitíveis e mais fáceis de se entender.

- **Write readably**: Tente não escrever parágrafos muito longos, que dificultem o acompanhamento do raciocínio. Utilize hyperlinks. Não utilize siglas obscuras sem a devida contextualização. Leia o documento após o término da escrita!!
